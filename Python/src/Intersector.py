from enum import Enum
from src.Point import Point
import numpy as np

class Position(Enum):
    Begin = 0
    Inner = 1
    End = 2
    Outer = 3


class Intersector:
    def __init__(self):
        self.positionIntersectionEdge = Position #posizione dell'intersezione
        self.toll = 1e-07 #tolleranza
        self.point = Point(0.0, 0.0)
        self.__resultParametricCoordinates = np.zeros(2) #coordinate parametriche per l'intersezione
        self.__originSegment = np.zeros(2) #punto di origine del segmento
        self.__rightHandSide = np.zeros(2) #vettore differenza tra punto di origine del lato e punto di origine del segmento
        temp = (2,2)
        self.__matrixTangentVector = np.zeros(temp)  #matrice che ha come colonne le direzioni delle due rette che si intersecano


    def ComputeIntersection(self) -> bool:
        parallelism = np.linalg.det(self.__matrixTangentVector)
        intersection: bool = False
        check = self.toll * self.toll * np.linalg.norm(self.__matrixTangentVector[:, 0]) * np.linalg.norm(self.__matrixTangentVector[:, 0]) \
                * np.linalg.norm(self.__matrixTangentVector[:, 1]) * np.linalg.norm(self.__matrixTangentVector[:, 1])
        # si usa la norma al quadrato per evitare la radice che è un'operazione costosa
        # quindi, dati due vettori v e w: check = tolleranza^2 * ||v||^2 * ||w||^2

        if parallelism * parallelism >= check: # i due vettori direzione non sono paralleli
            solverMatrix = np.array([[self.__matrixTangentVector[1, 1], - self.__matrixTangentVector[0, 1]],
                                    [-self.__matrixTangentVector[1, 0], self.__matrixTangentVector[0, 0]]])
            # sarebbe l'inversa di _matrixTangentVector
            self.__resultParametricCoordinates = np.dot(solverMatrix, self.__rightHandSide) # s = A^-1 * b
            self.__resultParametricCoordinates /= parallelism  #dividiamo perché abbiamo preso la matrice inversa
            if (self.__resultParametricCoordinates[1] > -self.toll and self.__resultParametricCoordinates[1]- 1.0 < self.toll): # 0<s2<1
                intersection = True

        else: # i due vettori non sono paralleli

            #determinante della matrice avente come prima colonna la direzione della retta
            # e come seconda colonna _rightHandSide

            parallelism2 = np.fabs(self.__matrixTangentVector[0, 0] * self.__rightHandSide[1] - self.__rightHandSide[0] *
                        self.__matrixTangentVector[1, 0])
            squaredNormFirstEdge = np.linalg.norm(self.__matrixTangentVector[:, 0]) * np.linalg.norm(self.__matrixTangentVector[:, 0])
            check2 = self.toll * self.toll * squaredNormFirstEdge * np.linalg.norm(self.__rightHandSide) * np.linalg.norm(self.__rightHandSide)
            if (parallelism2 * parallelism2 <= check2): #direzione della retta e _rightHandSide sono paralleli
                tempNorm = 1.0 / squaredNormFirstEdge
                self.__resultParametricCoordinates[0] = np.inner(self.__matrixTangentVector[:, 0], self.__rightHandSide) * tempNorm
                self.__resultParametricCoordinates[1] = self.__resultParametricCoordinates[0] - np.inner(self.__matrixTangentVector[:, 0], self.__matrixTangentVector[:, 1]) * tempNorm
                intersection = True

                if (self.__resultParametricCoordinates[1] < self.__resultParametricCoordinates[0]):
                    tmp = self.__resultParametricCoordinates[0]
                    self.__resultParametricCoordinates[0] = self.__resultParametricCoordinates[1]
                    self.__resultParametricCoordinates[1] = tmp

        # da qui in poi controlliamo la posizione
        if (self.__resultParametricCoordinates[1] < -self.toll or self.__resultParametricCoordinates[1] > 1.0 + self.toll):
            self.positionIntersectionEdge = Position.Outer
            intersection = False;
        elif ((self.__resultParametricCoordinates[1] > -self.toll) and (self.__resultParametricCoordinates[1] < self.toll)):
            self.__resultParametricCoordinates[1] = 0.0
            self.positionIntersectionEdge = Position.Begin
        elif ((self.__resultParametricCoordinates[1] > 1.0 - self.toll) and (self.__resultParametricCoordinates[1] <= 1.0 + self.toll)):
            self.__resultParametricCoordinates[1] = 1.0
            self.positionIntersectionEdge = Position.End
        else:
            self.positionIntersectionEdge = Position.Inner

        return intersection

    def SetSegment(self, origin: Point, end: Point):
        difference: Point = self.point.Dif(end, origin) #vettore "direzione" del segmento
        temp = np.array([difference.x, difference.y])
        self.__matrixTangentVector[:, 0] = temp
        temp2 = np.array([origin.x, origin.y])
        self.__originSegment = temp2

    def SetEdge(self, origin: Point, end: Point):
        difference: Point = self.point.Dif(origin, end) #vettore "direzione" del lato cambiato di segno
        temp = np.array([difference.x, difference.y])
        self.__matrixTangentVector[:, 1] = temp
        temp2 = np.array([origin.x, origin.y])
        self.__rightHandSide = temp2 - self.__originSegment #vettore differenza tra punto di origine del lato e punto di origine
                                                            #del segmento

    def IntersectionCoordinate(self, origin, end) -> Point: #sostituiamo il valore di s nella retta e troviamo le
                                                            #coordinate del punto di intersezione
        temp = np.array([end.x, end.y])
        temp2 = np.array([origin.x, origin.y])
        result = (1 - self.__resultParametricCoordinates[0]) * temp2 + self.__resultParametricCoordinates[0] * temp
        _point = Point(result[0], result[1])
        return _point

    def VectorialProduct(self, point1: Point, point2: Point):
        product = point1.x * point2.y - point1.y * point2.x
        return product