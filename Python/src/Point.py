class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def Dif(self, point1,point2):#calcola il vettore differenza tra due punti
                                 #(visti come vettori che partono dall'origine)
        dif = Point(point1.x - point2.x, point1.y - point2.y)
        return dif

    def __eq__(self,other):#overloading operatore ==
        return self.x == other.x and self.y == other.y

    def __lt__(self,other):#operatore che ordina i punti per ascissa decrescente
                           #(nel caso di ascisse uguali si ordina per ordinata decrescente)
       if self.x != other.x:
           return self.x > other.x
       else:
           return self.y > other.y

    def __ne__(self,other):#overloading operatore !=
        return self.x!=other.x or self.y!=other.y