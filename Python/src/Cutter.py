from src.Intersector import Intersector
from src.Intersector import Position
from src.Point import Point
import numpy as np


class Cutter(Intersector):
    def __init__(self):
        super(Cutter,self).__init__()
        self.__numSeg0 = 0 #intero corrispondente del primo punto del segmento
        self.__numSeg1 = 0 #intero corrispondente del secondo punto del segmento
        self.__cuttedPolygons = {}
        self.__newPoints = []
        self.__numNewPoints = []
        self.__intersections = []
        self.__numIntersec = []
        self.__controllo = []  #se le intersezioni sono un vertice metto (1), se no metto (0)


    def GetIntersections(self, vertices: [], vertexPosition: [], segment: []):
        intersector = Intersector()
        numVertices = len(vertices)
        numintersezioni = numVertices #numero da cui iniziamo a contare le intersezioni

        for i in range(0, numVertices): #pushiamo i vertici e il numero dei vertici
            self.__newPoints.append(vertices[i])
            self.__numNewPoints.append(vertexPosition[i])

        it_punti = 0
        it_numeri = 0
        v = 0

        while True:
            vertex1 = vertices[v]
            vertice2 = v+1
            if vertice2 >= numVertices: #serve per l'ultimo lato
                vertice2 = 0
            vertex2 = vertices[vertice2]
            intersector.SetSegment(segment[0], segment[1])
            intersector.SetEdge(vertex1, vertex2)
            it_punti += 1
            it_numeri += 1
            if intersector.ComputeIntersection() == True:
                if intersector.positionIntersectionEdge != Position.Begin and intersector.positionIntersectionEdge != Position.End:
                    #inseriamo l'intersezione tra i due vertici
                    self.__newPoints.insert(it_punti, intersector.IntersectionCoordinate(segment[0], segment[1]))
                    self.__intersections.append(intersector.IntersectionCoordinate(segment[0], segment[1]))
                    self.__numNewPoints.insert(it_numeri,numintersezioni)
                    self.__numIntersec.append(numintersezioni)
                    self.__controllo.append(0) #l'intersezione non cade su un vertice
                    numintersezioni += 1
                    it_punti += 1
                    it_numeri += 1

                #negli altri casi l'intersezione è già un vertice, non la pushiamo tra i newPoints
                elif intersector.positionIntersectionEdge == Position.End:
                    self.__intersections.append(vertex2)
                    self.__numIntersec.append(vertexPosition[vertice2])
                    self.__controllo.append(1) #l'intersezione cade su un vertice
                    v += 1
                    it_punti += 1
                    it_numeri += 1
                elif intersector.positionIntersectionEdge == Position.Begin:
                    self.__intersections.append(vertex1)
                    self.__numIntersec.append(vertexPosition[v])
                    self.__controllo.append(1) #l'intersezione cade su un vertice
                    v += 1
                    it_punti += 1
                    it_numeri += 1
            v +=1
            if v == numVertices:
                break

        #aggiungiamo i due punti del segmento
        self.__numSeg0 = numintersezioni
        numintersezioni += 1
        self.__numSeg1 = numintersezioni
        self.__intersections.sort() #ordiniamo le intersezioni per ascissa decrescente

    def GetCutGeneral(self, vertices: [], vertexPosition: [], segment: []) -> {}:
        it_punti = 0
        it_numeri = 0
        subPolygonId = 0 #per contare il numero di sottopoligoni
        counter = [] #per memorizzare da dove ho iniziato a percorrere il segmento
        numtotVertices = len(vertices)
        contatoreVer = 0 #numero di vertici memorizzati
        vertice = 0 #numero del vertice corrente
        vettSeg = self.point.Dif(segment[1], segment[0]) #"direzione" del segmento
        intersector = Intersector()
        flagSeg = np.zeros(2)
        intersector.SetSegment(segment[0], segment[1])

        while True:
            verticeInizio = vertice #vertice da dove inizia il sottopoligono
            self.__cuttedPolygons[subPolygonId] = []
            cuttedPolygons = self.__cuttedPolygons[subPolygonId]

            while True:
                cuttedPolygons.append(vertexPosition[vertice]) #push vertice iniziale
                vertice += 1
                contatoreVer += 1  # perchè memorizzo un vertice
                if self.__numNewPoints[it_numeri] == self.__numNewPoints[-1]:
                    it_numeri = 0
                    it_punti = 0
                else:
                    it_punti += 1
                    it_numeri += 1
                intersec = 0
                itIntersec = 0
                find = False

                while True:
                    if self.__newPoints[it_punti] == self.__intersections[intersec]:
                        find = True

                        while self.__numNewPoints[it_numeri] != self.__numIntersec[itIntersec]:
                            itIntersec += 1

                        if self.__controllo[itIntersec] == 1: #controlliamo se l'intersezione è in un vertice
                            vertice += 1
                        counter.append(vertice)  # memorizzo da dove ho iniziato a percorrere il segmento
                        cuttedPolygons.append(self.__numIntersec[itIntersec])#pushiamo il numero dell'intersezione

                    else:
                        intersec += 1

                    if intersec == len(self.__intersections) or find == True:
                        break

                #vediamo ora in che direzione del segmento spostarci
                if find == True:
                    it_punti -= 1 #controllo se il punto precedente all'intersezione è a sinistra o a destra
                    vett = self.point.Dif(self.__newPoints[it_punti], segment[0])
                    it_punti += 1
                    while True:
                        inter1 = self.__intersections[intersec]
                        flag = True #serve per capire se dobbiamo ripercorrere il segmento (false) o no (true)
                        flag2 = True  # serve per capire se si arriva da destra (true) o da sinistra (false)
                        itIntersec = 0
                        if intersector.VectorialProduct(vettSeg,vett) < -self.toll:  # se è a destra allora prendo l'intersezione sucessiva (mi sposto indietro)
                            intersec += 1
                        elif intersector.VectorialProduct(vettSeg,vett) > self.toll:  # se è a sinistra allora prendo l'intersezione precedente (mi sposto avanti)
                            intersec -= 1
                            flag2 = False

                        while True:
                            if self.__numNewPoints[it_numeri] == self.__numNewPoints[-1]:
                                it_numeri = 0
                                it_punti = 0
                            else:
                                it_punti += 1
                                it_numeri += 1

                            if self.__newPoints[it_punti] == self.__intersections[intersec]:
                                break

                        while self.__numNewPoints[it_numeri] != self.__numIntersec[itIntersec]:
                            itIntersec += 1

                        inter2 = self.__intersections[intersec]
                        if (segment[0] < inter1 and inter2 < segment[0]) or\
                           (segment[0] < inter2 and inter1 < segment[0]):  # Se S1 e S2 sono esterni
                            #primo punto del segmento compreso tra le due intersezioni
                            cuttedPolygons.append(self.__numSeg0)
                            flagSeg[0] = 1
                        if (segment[1] < inter1 and inter2 < segment[1]) or\
                           (segment[1] < inter2 and inter1 < segment[1]):
                            #secondo punto del segmento compreso tra le due intersezioni
                            cuttedPolygons.append(self.__numSeg1)
                            flagSeg[1] = 1

                        cuttedPolygons.append(self.__numIntersec[itIntersec])
                        if self.__controllo[itIntersec] == 1:
                            it_punti += 1
                            vett2 = self.point.Dif(self.__newPoints[it_punti], segment[0])
                            it_punti -= 1
                            if intersec == len(self.__intersections) or intersec == 0:
                                contatoreVer += 1
                            elif intersector.VectorialProduct(vettSeg, vett2) > self.toll and flag2 == True:
                                flag = False
                                contatoreVer += 1
                            elif intersector.VectorialProduct(vettSeg, vett2) < -self.toll and flag2 == False:
                                flag = False
                                contatoreVer += 1
                            elif intersector.VectorialProduct(vettSeg, vett2) < self.toll and intersector.VectorialProduct(vettSeg,
                                                                                                       vett2) > -self.toll:
                                #il punto successivo è ancora un'intersezione
                                flag = False
                                contatoreVer += 1
                                it_numeri += 2
                                counter.append(self.__numNewPoints[it_numeri])
                                it_numeri -= 2

                        if flag == True:
                            break

                    if self.__numNewPoints[it_numeri] == self.__numNewPoints[-1]:
                        it_numeri = 0
                        it_punti = 0
                    else:
                        it_punti += 1
                        it_numeri += 1
                    vertice = self.__numNewPoints[it_numeri]

                if vertice >= numtotVertices: #per l'ultimo lato
                    vertice = 0

                if vertice == verticeInizio:
                    break

            subPolygonId += 1
            it_punti = 0
            it_numeri = 0
            it_count = 0
            while True:
                it_punti += 1
                it_numeri += 1
                if counter[it_count] == self.__numNewPoints[it_numeri]:
                    break
            vertice = counter[it_count]
            counter.pop(0) #rimuoviamo l'elemento dopo averlo  messo in "vertice"

            if contatoreVer >= numtotVertices:
                break
#se i punti del segmento sono dentro al poligono i flag sono uguali a 1 e quindi putshiamo i punti nei newPoints
        if flagSeg[0] == 1:
            self.__newPoints.append(segment[0])
        if flagSeg[1] == 1:
            self.__newPoints.append(segment[1])

        return self.__cuttedPolygons


    def GetCutConvex(self, vertices: [], vertexPosition: [], segment: []) -> {}:
        intersector = Intersector()
        subPolygonId = 0
        subPolygonId1 = 1
        self.__cuttedPolygons[subPolygonId] = []
        self.__cuttedPolygons[subPolygonId1] = []
        cuttedPolygons = self.__cuttedPolygons[subPolygonId] #vettore estratto dalla mappa
        cuttedPolygons1 = self.__cuttedPolygons[subPolygonId1] #vettore estratto dalla mappa
        numVertices = len(vertices)
        counter = 0 #contatore delle intersezioni
        i = 0 #per il numero di ogni punto
        intersector.SetSegment(segment[0], segment[1])
        for k in range(0, numVertices):
            self.__newPoints.append(vertices[k])#pushiamo i vertici

        numIntersec = len(vertices) #numero da cui iniziamo a contare le intersezioni
        while True:
            cuttedPolygons.append(vertexPosition[i]) #pushiamo il vertice iniziale
            if i < len(vertices) - 1:
                intersector.SetEdge(vertices[i], vertices[i + 1])
            else:
                intersector.SetEdge(vertices[i], vertices[0])
            if counter == 0:
                if intersector.ComputeIntersection() == True:
                    counter = 1
                    pointInt1: Point = intersector.IntersectionCoordinate(segment[0], segment[1])
                    if intersector.positionIntersectionEdge != Position.Begin and intersector.positionIntersectionEdge != Position.End:
                        self.__newPoints.append(pointInt1)
                        cuttedPolygons1.append(numIntersec)
                        cuttedPolygons.append(numIntersec)
                        numIntersec += 1
                    else: #intersezione su un vertice, non la pushiamo nei newPoints
                        i += 1
                        cuttedPolygons.append(vertexPosition[i])
                        cuttedPolygons1.append(vertexPosition[i])
                    while True:
                        i += 1
                        cuttedPolygons1.append(vertexPosition[i]) #vertice che si trova al di là dell'intersezione
                        if i < len(vertices) - 1:
                            intersector.SetEdge(vertices[i], vertices[i + 1])
                        else:
                            intersector.SetEdge(vertices[i], vertices[0])
                        if intersector.ComputeIntersection() == True:
                            break

                    pointInt2: Point = intersector.IntersectionCoordinate(segment[0], segment[1])
                    k = 0
                    flag = 0
                    if intersector.positionIntersectionEdge != Position.Begin and intersector.positionIntersectionEdge != Position.End:
                        self.__newPoints.append(pointInt2)
                        k = numIntersec #servirà dopo per "chiudere" il primo sottopoligono
                        flag = 1
                        cuttedPolygons1.append(numIntersec)

                    #controlliamo se i punti del segmento sono dentro al poligono
                    if (segment[0] < pointInt1 and pointInt2 < segment[0]) or (
                            segment[0] < pointInt2 and pointInt1 < segment[0]):
                        numIntersec += 1
                        cuttedPolygons1.append(numIntersec)
                        self.__newPoints.append(segment[0])
                    if (segment[1] < pointInt1 and pointInt2 < segment[1]) or (
                        segment[1] < pointInt2 and pointInt1 < segment[1]):
                        numIntersec += 1
                        cuttedPolygons1.append(numIntersec)
                        cuttedPolygons.append(numIntersec)
                        self.__newPoints.append(segment[1])
                    numIntersec -= 1
                    cuttedPolygons.append(numIntersec)
                    #aggiunto dopo perché nel primo sottopoligono prima ci va il punto del segmento con numero
                    #più alto e dopo quello più basso

                    if flag == 1:
                        cuttedPolygons.append(k)

            i += 1
            if i == numVertices:
                break
        return self.__cuttedPolygons

    def GetNewPointsReturn(self): #metodo di supporto che ritorna i newPoints
        return self.__newPoints