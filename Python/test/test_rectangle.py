from unittest import TestCase
from src.Cutter import Cutter
from src.Point import Point


class TestRectangle(TestCase):
    @staticmethod
    def FillRectangleVertices() -> []:
        vertices = [Point(1.0000e+00, 1.0000e+00),
                    Point(5.0000e+00, 1.0000e+00),
                    Point(5.0000e+00, 3.1000e+00),
                    Point(1.0000e+00, 3.1000e+00)]
        return vertices

    @staticmethod
    def FillVertexPosition() -> []:
        vertexPosition = [int(0),
                          int(1),
                          int(2),
                          int(3)]
        return vertexPosition

    @staticmethod
    def FillSegment() -> []:
        segment = [Point(2.0000e+00, 1.2000e+00),
                   Point(4.0000e+00, 3.0000e+00)]
        return segment

    @staticmethod
    def FillNewPoints1() -> []:
        newPoints = [Point(1.0000e+00, 1.0000e+00),
                     Point(1.77778e+00, 1.0000e+00),
                     Point(5.0000e+00, 1.0000e+00),
                     Point(5.0000e+00, 3.1000e+00),
                     Point(4.11111e+00, 3.1000e+00),
                     Point(1.0000e+00, 3.1000e+00),
                     Point(2.0000e+00, 1.2000e+00),
                     Point(4.0000e+00, 3.0000e+00)]

        return newPoints

    @staticmethod
    def FillCuttedPolygons() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 4, 7, 6, 5, 3]
        cuttedPolygons[1] = [4, 1, 2, 5, 6, 7]

        return cuttedPolygons

    def test_GetNewPoints(self):
        P = Point(0,0)
        C = Cutter()
        vertices = TestRectangle.FillRectangleVertices()
        vertexPosition = TestRectangle.FillVertexPosition()
        segment = TestRectangle.FillSegment()
        C.GetIntersections(vertices, vertexPosition, segment)
        C.GetCutGeneral(vertices, vertexPosition, segment)
        newPoints1 = TestRectangle.FillNewPoints1()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 8):
           self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
           self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutConvex(self):
        vertices = TestRectangle.FillRectangleVertices()
        vertexPosition = TestRectangle.FillVertexPosition()
        segment = TestRectangle.FillSegment()
        cuttedPolygons1 = TestRectangle.FillCuttedPolygons()
        cutter = Cutter()
        cuttedPolygons2 = cutter.GetCutConvex(vertices, vertexPosition, segment)

        for i in range(0, 2):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 6):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)