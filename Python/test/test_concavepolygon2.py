from unittest import TestCase
from src.Cutter import Cutter
from src.Point import Point


class TestConcavePolygon2(TestCase):
    @staticmethod
    def FillConcavePolygonVertices() -> []:
        vertices = [Point(2.0000e+00, -2.0000e+00),
                    Point(0.0000e+00, -1.0000e+00),
                    Point(3.0000e+00, 1.0000e+00),
                    Point(0.0000e+00, 2.0000e+00),
                    Point(3.0000e+00, 2.0000e+00),
                    Point(3.0000e+00, 3.0000e+00),
                    Point(-1.0000e+00, 3.0000e+00),
                    Point(-3.0000e+00, 1.0000e+00),
                    Point(0.0000e+00, 0.0000e+00),
                    Point(-3.0000e+00, -2.0000e+00)]
        return vertices

    @staticmethod
    def FillVertexPosition() -> []:
        vertexPosition = [int(0),
                          int(1),
                          int(2),
                          int(3),
                          int(4),
                          int(5),
                          int(6),
                          int(7),
                          int(8),
                          int(9)]
        return vertexPosition

    @staticmethod
    def FillSegment1() -> []:
        segment = [Point(-4.0000e+00, -4.0000e+00),
                   Point(4.0000e+00, 4.0000e+00)]
        return segment

    @staticmethod
    def FillNewPoints1() -> []:
        newPoints = [Point(2.0000e+00, -2.0000e+00),
                    Point(0.0000e+00, -1.0000e+00),
                    Point(3.0000e+00, 1.0000e+00),
                    Point(1.5000e+00, 1.5000e+00),
                    Point(0.0000e+00, 2.0000e+00),
                    Point(2.0000e+00, 2.0000e+00),
                    Point(3.0000e+00, 2.0000e+00),
                    Point(3.0000e+00, 3.0000e+00),
                    Point(-1.0000e+00, 3.0000e+00),
                    Point(-3.0000e+00, 1.0000e+00),
                    Point(0.0000e+00, 0.0000e+00),
                    Point(-3.0000e+00, -2.0000e+00),
                    Point(-2.0000e+00, -2.0000e+00)]

        return newPoints

    @staticmethod
    def FillCuttedPolygons1() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 1, 2, 10, 8, 12]
        cuttedPolygons[1] = [3, 11, 5, 6, 7, 8, 10]
        cuttedPolygons[2] = [4, 5, 11]
        cuttedPolygons[3] = [9, 12, 8]

        return cuttedPolygons

    def test_GetNewPoints1(self):
        P = Point(0, 0)
        C = Cutter()
        vertices = TestConcavePolygon2.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon2.FillVertexPosition()
        segment = TestConcavePolygon2.FillSegment1()
        C.GetIntersections(vertices, vertexPosition, segment)
        C.GetCutGeneral(vertices, vertexPosition, segment)
        newPoints1 = TestConcavePolygon2.FillNewPoints1()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 13):
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutGeneral1(self):
        vertices = TestConcavePolygon2.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon2.FillVertexPosition()
        segment = TestConcavePolygon2.FillSegment1()
        cuttedPolygons1 = TestConcavePolygon2.FillCuttedPolygons1()
        cutter = Cutter()
        cutter.GetIntersections(vertices, vertexPosition, segment)
        cuttedPolygons2 = cutter.GetCutGeneral(vertices, vertexPosition, segment)

        for i in range(0, 1):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 6):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)

        for i in range(1, 2):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 7):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)

        for i in range(2, 4):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 3):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)

    @staticmethod
    def FillSegment2() -> []:
        segment = [Point(0.0000e+00, -3.0000e+00),
                   Point(0.0000e+00, 4.0000e+00)]
        return segment

    @staticmethod
    def FillNewPoints2() -> []:
        newPoints = [Point(2.0000e+00, -2.0000e+00),
                     Point(0.0000e+00, -1.0000e+00),
                     Point(3.0000e+00, 1.0000e+00),
                     Point(0.0000e+00, 2.0000e+00),
                     Point(3.0000e+00, 2.0000e+00),
                     Point(3.0000e+00, 3.0000e+00),
                     Point(0.0000e+00, 3.0000e+00),
                     Point(-1.0000e+00, 3.0000e+00),
                     Point(-3.0000e+00, 1.0000e+00),
                     Point(0.0000e+00, 0.0000e+00),
                     Point(-3.0000e+00, -2.0000e+00),
                     Point(0.0000e+00, -2.0000e+00)]

        return newPoints

    @staticmethod
    def FillCuttedPolygons2() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 1, 11]
        cuttedPolygons[1] = [2, 3, 8, 1]
        cuttedPolygons[2] = [4, 5, 10, 3]
        cuttedPolygons[3] = [6, 7, 8, 3, 10]
        cuttedPolygons[4] = [9, 11, 1, 8]

        return cuttedPolygons

    def test_GetNewPoints2(self):
        P = Point(0, 0)
        C = Cutter()
        vertices = TestConcavePolygon2.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon2.FillVertexPosition()
        segment = TestConcavePolygon2.FillSegment2()
        C.GetIntersections(vertices, vertexPosition, segment)
        C.GetCutGeneral(vertices, vertexPosition, segment)
        newPoints1 = TestConcavePolygon2.FillNewPoints2()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 12):
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutGeneral2(self):
        vertices = TestConcavePolygon2.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon2.FillVertexPosition()
        segment = TestConcavePolygon2.FillSegment2()
        cuttedPolygons1 = TestConcavePolygon2.FillCuttedPolygons2()
        cutter = Cutter()
        cutter.GetIntersections(vertices, vertexPosition, segment)
        cuttedPolygons2 = cutter.GetCutGeneral(vertices, vertexPosition, segment)

        cuttedPolygonsVect1 = cuttedPolygons1[0]
        cuttedPolygonsVect2 = cuttedPolygons2[0]
        for j in range(0, 3):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        for i in range(1, 3):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 4):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)

        cuttedPolygonsVect1 = cuttedPolygons1[3]
        cuttedPolygonsVect2 = cuttedPolygons2[3]
        for j in range(0, 5):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        cuttedPolygonsVect1 = cuttedPolygons1[4]
        cuttedPolygonsVect2 = cuttedPolygons2[4]
        for j in range(0, 4):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)