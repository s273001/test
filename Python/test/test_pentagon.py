from unittest import TestCase
from src.Cutter import Cutter
from src.Point import Point


class TestPentagon(TestCase):
    @staticmethod
    def FillPentagonVertices() -> []:
        vertices = [Point(2.5000e+00, 1.0000e+00),
                    Point(4.0000e+00, 2.1000e+00),
                    Point(3.4000e+00, 4.2000e+00),
                    Point(1.6000e+00, 4.2000e+00),
                    Point(1.0000e+00, 2.1000e+00)]
        return vertices

    @staticmethod
    def FillVertexPosition() -> []:
        vertexPosition = [int(0),
                          int(1),
                          int(2),
                          int(3),
                          int(4),
                          int(5)]
        return vertexPosition

    @staticmethod
    def FillSegment() -> []:
        segment = [Point(1.4000e+00, 2.7500e+00),
                   Point(3.6000e+00, 2.2000e+00)]
        return segment

    @staticmethod
    def FillNewPoints1() -> []:
        newPoints = [Point(2.5000e+00, 1.0000e+00),
                    Point(4.0000e+00, 2.1000e+00),
                    Point(3.4000e+00, 4.2000e+00),
                    Point(1.6000e+00, 4.2000e+00),
                    Point(1.0000e+00, 2.1000e+00),
                    Point(1.2000e+00, 2.8000e+00),
                    Point(1.4000e+00, 2.7500e+00),
                    Point(3.6000e+00, 2.2000e+00)]

        return newPoints

    @staticmethod
    def FillCuttedPolygons() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 1, 7, 6, 5, 4]
        cuttedPolygons[1] = [1, 2, 3, 5, 6, 7]

        return cuttedPolygons

    def test_GetNewPoints(self):
        P = Point(0,0)
        C = Cutter()
        vertices = TestPentagon.FillPentagonVertices()
        vertexPosition = TestPentagon.FillVertexPosition()
        segment = TestPentagon.FillSegment()
        C.GetCutConvex(vertices, vertexPosition, segment)
        newPoints1 = TestPentagon.FillNewPoints1()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 8):
           self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
           self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutConvex(self):
        vertices = TestPentagon.FillPentagonVertices()
        vertexPosition = TestPentagon.FillVertexPosition()
        segment = TestPentagon.FillSegment()
        cuttedPolygons1 = TestPentagon.FillCuttedPolygons()
        cutter = Cutter()
        cuttedPolygons2 = cutter.GetCutConvex(vertices, vertexPosition, segment)

        for i in range(0, 2):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 6):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)