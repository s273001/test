from unittest import TestCase
from src.Intersector import Intersector
from src.Point import Point


class TestIntersector(TestCase):
    def test_Intersection(self):
        a = Point(1.0000e+00, 1.0000e+00)
        b = Point(5.0000e+00, 1.0000e+00)
        c = Point(2.0000e+00, 1.2000e+00)
        d = Point(4.0000e+00, 3.0000e+00)
        intersector = Intersector()
        intersector.SetEdge(a, b)
        intersector.SetSegment(c, d)
        self.assertTrue(intersector.ComputeIntersection())
        e = Point(5.0000e+00, 1.0000e+00)
        f = Point(5.0000e+00, 3.1000e+00)
        intersector.SetEdge(e, f)
        self.assertFalse(intersector.ComputeIntersection())

    def test_IntersectionCoordinate(self):
        a = Point(2.0000e+00, 1.2000e+00)
        b = Point(4.0000e+00, 3.0000e+00)
        c = Point(1.0000e+00, 1.0000e+00)
        d = Point(5.0000e+00, 1.0000e+00)
        intersector = Intersector()
        intersector.SetSegment(a, b)
        intersector.SetEdge(c, d)
        self.assertTrue(intersector.ComputeIntersection())
        self.assertTrue(abs(intersector.IntersectionCoordinate(a, b).y - 1.00000e+00) < 1e-4)
        self.assertTrue(abs(intersector.IntersectionCoordinate(a, b).x - 1.77777e+00) < 1e-4)

    def test_VectorialProduct(self):
        a = Point(1.0000e+00, 1.0000e+00)
        b = Point(5.0000e+00, 1.0000e+00)
        intersector = Intersector()
        self.assertEqual(intersector.VectorialProduct(a, b), -4.0000e+00)