from unittest import TestCase
from src.Point import Point


class TestPoint(TestCase):
    def test_Dif(self):
        p1 = Point(1.000e+00, 1.000e+00)
        p2 = Point(5.000e+00, 1.000e+00)
        p3 = Point(0,0)
        self.assertEqual(Point(-4.000e+00, 0.000e+00),p3.Dif(p1,p2))