from unittest import TestCase
from src.Cutter import Cutter
from src.Point import Point


class TestConcavePolygon(TestCase):
    @staticmethod
    def FillConcavePolygonVertices() -> []:
        vertices = [Point(1.5000e+00, 1.0000e+00),
                    Point(5.6000e+00, 1.5000e+00),
                    Point(5.5000e+00, 4.8000e+00),
                    Point(4.0000e+00, 6.2000e+00),
                    Point(3.2000e+00, 4.2000e+00),
                    Point(1.0000e+00, 4.0000e+00)]
        return vertices

    @staticmethod
    def FillVertexPosition() -> []:
        vertexPosition = [int(0),
                          int(1),
                          int(2),
                          int(3),
                          int(4),
                          int(5)]
        return vertexPosition

    @staticmethod
    def FillSegment() -> []:
        segment = [Point(2.0000e+00, 3.7000e+00),
                   Point(4.1000e+00, 5.9000e+00)]
        return segment

    @staticmethod
    def FillNewPoints1() -> []:
        newPoints = [Point(1.5000e+00, 1.0000e+00),
                    Point(5.6000e+00, 1.5000e+00),
                    Point(5.5000e+00, 4.8000e+00),
                    Point(4.20433e+00, 6.00929e+00),
                    Point(4.0000e+00, 6.2000e+00),
                    Point(3.72131e+00, 5.50328e+00),
                    Point(3.2000e+00, 4.2000e+00),
                    Point(2.40860e+00, 4.12805e+00),
                    Point(1.0000e+00, 4.0000e+00),
                    Point(1.19122e+00, 2.85270e+00),
                    Point(2.0000e+00, 3.7000e+00),
                    Point(4.1000e+00, 5.9000e+00)]

        return newPoints

    @staticmethod
    def FillCuttedPolygons() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 1, 2, 6, 11, 7, 4, 8, 10, 9]
        cuttedPolygons[1] = [3, 7, 11, 6]
        cuttedPolygons[2] = [5, 9, 10, 8]

        return cuttedPolygons

    def test_GetNewPoints(self):
        P = Point(0, 0)
        C = Cutter()
        vertices = TestConcavePolygon.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon.FillVertexPosition()
        segment = TestConcavePolygon.FillSegment()
        C.GetIntersections(vertices, vertexPosition, segment)
        C.GetCutGeneral(vertices, vertexPosition, segment)
        newPoints1 = TestConcavePolygon.FillNewPoints1()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 12):
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutGeneral(self):
        vertices = TestConcavePolygon.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon.FillVertexPosition()
        segment = TestConcavePolygon.FillSegment()
        cuttedPolygons1 = TestConcavePolygon.FillCuttedPolygons()
        cutter = Cutter()
        cutter.GetIntersections(vertices, vertexPosition, segment)
        cuttedPolygons2 = cutter.GetCutGeneral(vertices, vertexPosition, segment)

        cuttedPolygonsVect1 = cuttedPolygons1[0]
        cuttedPolygonsVect2 = cuttedPolygons2[0]
        for j in range(0, 10):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        for i in range(1, 3):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 4):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)