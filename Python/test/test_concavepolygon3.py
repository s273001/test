from unittest import TestCase
from src.Cutter import Cutter
from src.Point import Point


class TestConcavePolygon3(TestCase):
    @staticmethod
    def FillConcavePolygonVertices() -> []:
        vertices = [Point(-1.0000e+00, 1.0000e+00),
                    Point(3.0000e+00, 1.0000e+00),
                    Point(4.0000e+00, 4.0000e+00),
                    Point(2.0000e+00, 4.0000e+00),
                    Point(2.5000e+00, 3.3000e+00),
                    Point(0.5000e+00, 3.0000e+00),
                    Point(0.0000e+00, 5.0000e+00),
                    Point(-1.3000e+00, 4.7000e+00)]
        return vertices

    @staticmethod
    def FillVertexPosition() -> []:
        vertexPosition = [int(0),
                          int(1),
                          int(2),
                          int(3),
                          int(4),
                          int(5),
                          int(6),
                          int(7)]
        return vertexPosition

    @staticmethod
    def FillSegment() -> []:
        segment = [Point(0.877356361e+00,3.0566034541e+00),
                   Point(4.988014965e+00, 3.6732022448e+00),]
        return segment

    @staticmethod
    def FillNewPoints1() -> []:
        newPoints = [Point(-1.0000e+00, 1.0000e+00),
                    Point(3.0000e+00, 1.0000e+00),
                    Point(3.833333e+00, 3.5000e+00),
                    Point(4.0000e+00, 4.0000e+00),
                    Point(2.0000e+00, 4.0000e+00),
                    Point(2.5000e+00, 3.3000e+00),
                    Point(0.5000e+00, 3.0000e+00),
                    Point(0.0000e+00, 5.0000e+00),
                    Point(-1.3000e+00, 4.7000e+00),
                    Point(-1.142189e+00, 2.753671e+00),
                    Point(0.877356361e+00,3.0566034541e+00)]

        return newPoints

    @staticmethod
    def FillCuttedPolygons() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 1, 8, 4, 10, 5, 9]
        cuttedPolygons[1] = [2, 3, 4, 8]
        cuttedPolygons[2] = [6, 7, 9, 5]

        return cuttedPolygons

    def test_GetNewPoints(self):
        P = Point(0, 0)
        C = Cutter()
        vertices = TestConcavePolygon3.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon3.FillVertexPosition()
        segment = TestConcavePolygon3.FillSegment()
        C.GetIntersections(vertices, vertexPosition, segment)
        C.GetCutGeneral(vertices, vertexPosition, segment)
        newPoints1 = TestConcavePolygon3.FillNewPoints1()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 11):
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutConvex(self):
        vertices = TestConcavePolygon3.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon3.FillVertexPosition()
        segment = TestConcavePolygon3.FillSegment()
        cuttedPolygons1 = TestConcavePolygon3.FillCuttedPolygons()
        cutter = Cutter()
        cutter.GetIntersections(vertices, vertexPosition, segment)
        cuttedPolygons2 = cutter.GetCutGeneral(vertices, vertexPosition, segment)

        cuttedPolygonsVect1 = cuttedPolygons1[0]
        cuttedPolygonsVect2 = cuttedPolygons2[0]
        for j in range(0, 7):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        for i in range(1, 3):
            cuttedPolygonsVect1 = cuttedPolygons1[i]
            cuttedPolygonsVect2 = cuttedPolygons2[i]
            for j in range(0, 4):
                point1 = cuttedPolygonsVect1[j]
                point2 = cuttedPolygonsVect2[j]
                self.assertEqual(point1, point2)