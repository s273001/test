from unittest import TestCase
from src.Cutter import Cutter
from src.Point import Point


class TestConcavePolygon4(TestCase):
    @staticmethod
    def FillConcavePolygonVertices() -> []:
        vertices = [Point(3.0000e+00, 0.0000e+00),
                    Point(2.0000e+00, 0.0000e+00),
                    Point(2.0000e+00, 3.0000e+00),
                    Point(-2.0000e+00, 1.0000e+00),
                    Point(-3.0000e+00, 2.0000e+00),
                    Point(-3.0000e+00, -4.0000e+00),
                    Point(-1.0000e+00, -5.0000e+00),
                    Point(-2.0000e+00, -2.0000e+00),
                    Point(1.0000e+00, 1.0000e+00),
                    Point(2.0000e+00, -4.0000e+00),
                    Point(4.0000e+00, -1.0000e+00),
                    Point(5.0000e+00, 3.0000e+00),
                    Point(3.0000e+00, 4.0000e+00)]
        return vertices

    @staticmethod
    def FillVertexPosition() -> []:
        vertexPosition = [int(0),
                          int(1),
                          int(2),
                          int(3),
                          int(4),
                          int(5),
                          int(6),
                          int(7),
                          int(8),
                          int(9),
                          int(10),
                          int(11),
                          int(12)]
        return vertexPosition

    @staticmethod
    def FillSegment() -> []:
        segment = [Point(0.0000e+00,0.0000e+00),
                   Point(3.5000e+00, 3.5000e+00),]
        return segment

    @staticmethod
    def FillNewPoints1() -> []:
        newPoints = [Point(3.0000e+00, 0.0000e+00),
                    Point(2.0000e+00, 0.0000e+00),
                    Point(2.0000e+00, 2.0000e+00),
                    Point(2.0000e+00, 3.0000e+00),
                    Point(-2.0000e+00, 1.0000e+00),
                    Point(-3.0000e+00, 2.0000e+00),
                    Point(-3.0000e+00, -3.0000e+00),
                    Point(-3.0000e+00, -4.0000e+00),
                    Point(-1.0000e+00, -5.0000e+00),
                    Point(-2.0000e+00, -2.0000e+00),
                    Point(1.0000e+00, 1.0000e+00),
                    Point(2.0000e+00, -4.0000e+00),
                    Point(4.0000e+00, -1.0000e+00),
                    Point(5.0000e+00, 3.0000e+00),
                    Point(3.66666e+00, 3.66666e+00),
                    Point(3.0000e+00, 4.0000e+00),
                    Point(3.0000e+00, 3.0000e+00),
                    Point(0.0000e+00, 0.0000e+00),
                    Point(3.5000e+00, 3.5000e+00),]

        return newPoints

    @staticmethod
    def FillCuttedPolygons() -> {}:
        cuttedPolygons = {}
        cuttedPolygons[0] = [0, 1, 13, 8, 9, 10, 11, 15, 18, 16]
        cuttedPolygons[1] = [2, 3, 4, 14, 7, 17, 8, 13]
        cuttedPolygons[2] = [12, 16, 18, 15]
        cuttedPolygons[3] = [5, 6, 7, 14]

        return cuttedPolygons

    def test_GetNewPoints(self):
        P = Point(0, 0)
        C = Cutter()
        vertices = TestConcavePolygon4.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon4.FillVertexPosition()
        segment = TestConcavePolygon4.FillSegment()
        C.GetIntersections(vertices, vertexPosition, segment)
        C.GetCutGeneral(vertices, vertexPosition, segment)
        newPoints1 = TestConcavePolygon4.FillNewPoints1()
        newPoints2 = C.GetNewPointsReturn()
        toll = 1.0e-5

        for i in range(0, 19):
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).x <= toll)
            self.assertTrue(P.Dif(newPoints1[i], newPoints2[i]).y <= toll)

    def test_GetCutConvex(self):
        vertices = TestConcavePolygon4.FillConcavePolygonVertices()
        vertexPosition = TestConcavePolygon4.FillVertexPosition()
        segment = TestConcavePolygon4.FillSegment()
        cuttedPolygons1 = TestConcavePolygon4.FillCuttedPolygons()
        cutter = Cutter()
        cutter.GetIntersections(vertices, vertexPosition, segment)
        cuttedPolygons2 = cutter.GetCutGeneral(vertices, vertexPosition, segment)

        cuttedPolygonsVect1 = cuttedPolygons1[0]
        cuttedPolygonsVect2 = cuttedPolygons2[0]
        for j in range(0, 10):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        cuttedPolygonsVect1 = cuttedPolygons1[1]
        cuttedPolygonsVect2 = cuttedPolygons2[1]
        for j in range(0, 8):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        cuttedPolygonsVect1 = cuttedPolygons1[2]
        cuttedPolygonsVect2 = cuttedPolygons2[2]
        for j in range(0, 4):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)

        cuttedPolygonsVect1 = cuttedPolygons1[3]
        cuttedPolygonsVect2 = cuttedPolygons2[3]
        for j in range(0, 4):
            point1 = cuttedPolygonsVect1[j]
            point2 = cuttedPolygonsVect2[j]
            self.assertEqual(point1, point2)