#include "Point.hpp"

namespace PolygonCut {

Point::Point(const double& x, const double& y)
{
  Point::x = x;
  Point::y = y;
}


Point Point::Dif(const Point& point1, const Point& point2)
{
    Point dif(point1.x-point2.x,point1.y-point2.y);
    return dif ;
}

}
