#ifndef POINT_H
#define POINT_H

namespace PolygonCut {

class Point {
public:
    double x;
    double y;

    Point() {x = 0.0; y = 0.0;}

    Point(const double& x, const double& y); //costruttore copia

    bool operator < (const Point& point)
    {
        if(x!=point.x)
            return x>point.x;
        else
            return y>point.y;
    }//operatore che ordina i punti per ascissa decrescente (nel caso di ascisse uguali si ordina per ordinata decrescente)

    bool operator == (const Point& point) const {return x==point.x &&y==point.y;}

    bool operator != (const Point& point) const {return x!=point.x || y!=point.y;}

    Point Dif (const Point& point1, const Point& point2);//calcola il vettore differenza tra due punti
                                                         //(visti come vettori che partono dall'origine)
};
}

#endif // POINT_H
