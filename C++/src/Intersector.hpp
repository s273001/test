#ifndef INTERSECTOR_HPP
#define INTERSECTOR_HPP

#include "Eigen"
#include "iostream"

#include <vector>
#include <list>
#include "Point.hpp"

using namespace std;
using namespace Eigen;

namespace PolygonCut{

class Intersector
{
public:

    enum class Position
           {
               Begin = 0,
               Inner = 1,
               End = 2,
               Outer = 3
           };

    Position positionIntersectionEdge;//posizione dell'intersezione
    double toll;//tolleranza
    Point point;


private:

    Vector2d _resultParametricCoordinates; //coordinate parametriche per l'intersezione
    Vector2d _originSegment; //punto di origine del segmento
    Vector2d _rightHandSide;//vettore differenza tra punto di origine del lato e punto di origine del segmento
    Matrix2d _matrixTangentVector; //matrice che ha come colonne le direzioni delle due rette che si intersecano

public:
    Intersector();

    void SetSegment(const Point& origin, const Point& end);

    void SetEdge(const Point& origin, const Point& end);

    bool ComputeIntersection();//calcolo dell'intersezione

    Point IntersectionCoordinate(const Point& origin,const Point& end);//calcolo del punto di intersezione

    double VectorialProduct(const Point& point1, const Point& point2);
    //prodotto vettoriale tra due punti (visti come vettori che partono dall'origine)
};

}

#endif // INTERSECTOR_H

