#include "Cutter.hpp"

namespace PolygonCut {

void Cutter::GetIntersections(const vector<Point> &vertices, const vector<int>& vertexPosition, const vector<Point> &segment) 
{
    Intersector intersector;
    unsigned int numVertices = vertices.size();
    unsigned int numintersezioni= numVertices;//numero da cui iniziamo a contare le intersezioni
    for (unsigned int i=0; i<numVertices; i++)//pushiamo i vertici e il numero dei vertici
    {
      _newPoints.push_back(vertices[i]);
      _numNewPoints.push_back(vertexPosition[i]);
    }
    list<Point>::iterator it_punti = _newPoints.begin();
    list<int>::iterator it_numeri = _numNewPoints.begin();
    for (unsigned int v = 0; v < numVertices; v++)
    {
      const Point& vertex1 = vertices[v];
      unsigned int vertice2=v+1;
      if(vertice2>=numVertices)//serve per l'ultimo lato
          vertice2=0;
      const Point& vertex2 = vertices[vertice2];
      intersector.SetSegment(segment[0],segment[1]);
      intersector.SetEdge(vertex1, vertex2);
      it_punti++;
      it_numeri++;
      if (intersector.ComputeIntersection() == true)
      {
          if(intersector.positionIntersectionEdge != Position::Begin && intersector.positionIntersectionEdge != Position::End)
          {
              //inseriamo l'intersezione tra i due vertici
              _newPoints.insert(it_punti,intersector.IntersectionCoordinate(segment[0],segment[1]));
              _intersections.push_back(intersector.IntersectionCoordinate(segment[0],segment[1]));
              _numNewPoints.insert(it_numeri,numintersezioni);
              _numIntersec.push_back(numintersezioni);
              _controllo.push_back(0);//l'intersezione non cade su un vertice
              numintersezioni++;
          }
          //negli altri casi l'intersezione è già un vertice, non la pushiamo tra i newPoints
          else if(intersector.positionIntersectionEdge == Position::End)
          {
              _intersections.push_back(vertex2);
              _numIntersec.push_back(vertexPosition[vertice2]);
              _controllo.push_back(1);//l'intersezione cade su un vertice
              v++;
              it_punti++;
              it_numeri++;
          }
          else if(intersector.positionIntersectionEdge == Position::Begin)
          {
              _intersections.push_back(vertex1);
              _numIntersec.push_back(vertexPosition[v]);
              _controllo.push_back(1);//l'intersezione cade su un vertice
              v++;
              it_punti++;
              it_numeri++;
          }
      }
    }
    //aggiungiamo i due punti del segmento
    _numSeg0=numintersezioni;
    numintersezioni++;
    _numSeg1=numintersezioni;
    _intersections.sort();//ordiniamo le intersezioni per ascissa decrescente

}


unordered_map<int, vector<int>> Cutter::GetCutGeneral(const vector<Point>& vertices, const vector<int> vertexPosition, const vector<Point> &segment)
{
    list<Point>::iterator it_punti = _newPoints.begin();
    list<int>::iterator it_numeri = _numNewPoints.begin();
    list<Point>::iterator intersec = _intersections.begin();
    unsigned int subPolygonId = 0; //per contare il numero di sottopoligoni
    list<int> counter; //per memorizzare da dove ho iniziato a percorrere il segmento
    list<int>::iterator it_count = counter.begin();
    unsigned int numtotVertices = vertices.size();
    unsigned int contatoreVer=0;//numero di vertici memorizzati
    unsigned int vertice = 0;//numero del vertice corrente
    bool find = false;
    Point vettSeg= point.Dif(segment[1],segment[0]);//"direzione" del segmento
    Intersector intersector;
    unsigned int flagSeg[2]={0,0};
    bool flag;
    int itIntersec =0; //indice del vettore numIntersec
    intersector.SetSegment(segment[0],segment[1]);
    Point Seg0(segment[0].x,segment[0].y);
    Point Seg1(segment[1].x,segment[1].y);

    do
    {
         unsigned int verticeinizio = vertice; //vertice da dove inizia il sottopoligono
         vector<int>& cuttedPolygons = _cuttedPolygons[subPolygonId];
         do
         {
             cuttedPolygons.push_back(vertexPosition[vertice]);//push vertice iniziale
             vertice++;
             contatoreVer++;// perchè memorizzo un vertice
             it_punti++;
             it_numeri++;
             intersec = _intersections.begin();
             itIntersec=0;
             find = false;
             do
             {
                 if(*it_punti==*intersec)
                 {
                     find = true;
                     while(*it_numeri !=_numIntersec[itIntersec] )
                     {
                         itIntersec++;
                     }
                     if(_controllo[itIntersec]==1)//controlliamo se l'intersezione è in un vertice
                         vertice++;
                     counter.push_back(vertice); //memorizzo da dove ho iniziato a percorrere il segmento
                     cuttedPolygons.push_back(_numIntersec[itIntersec]);//pushiamo il numero dell'intersezione
                 }
                 else
                   intersec++;

             }while(intersec != _intersections.end() && find != true);
             //vediamo ora in che direzione del segmento spostarci
             if(find == true)
             {
                 it_punti--;//  controllo se il punto precedente all'intersezione è a sinistra o a destra
                 Point vett=point.Dif(*it_punti,segment[0]);
                 it_punti++;
                 do{
                     Point inter1(*intersec);
                     flag=true; //serve per capire se dobbiamo ripercorrere il segmento (false) o no (true)
                     bool flag2= true;//serve per capire se si arriva da destra (true) o da sinistra (false)
                     itIntersec=0;
                     if(intersector.VectorialProduct(vettSeg,vett)<-toll) // se è a destra allora prendo l'intersezione sucessiva (mi sposto indietro)
                     {
                         intersec++;
                     }
                     else if(intersector.VectorialProduct(vettSeg,vett)>toll) // se è a sinistra allora prendo l'intersezione precedente (mi sposto avanti)
                     {
                         intersec--;
                         flag2= false;
                     }
                     do
                     {
                         if(it_numeri == _numNewPoints.end() )
                         {
                             it_numeri = _numNewPoints.begin();
                             it_punti = _newPoints.begin();
                         }
                         else
                         {
                             it_punti++;
                             it_numeri++;
                         }
                     }while(*it_punti!=*intersec);
                     while(*it_numeri != _numIntersec[itIntersec] )
                     {
                         itIntersec++;
                     }
                     Point inter2(*intersec);
                     if((Seg0 < inter1 && inter2 < Seg0) || (Seg0 < inter2 && inter1 < Seg0))
                     {  //primo punto del segmento compreso tra le due intersezioni
                         cuttedPolygons.push_back(_numSeg0);
                          flagSeg[0]=1;
                     }
                     if((Seg1 < inter1 && inter2 < Seg1) || (Seg1 < inter2 && inter1 < Seg1))
                     {  //secondo punto del segmento compreso tra le due intersezioni
                         cuttedPolygons.push_back(_numSeg1);
                         flagSeg[1]=1;
                     }
                     cuttedPolygons.push_back(_numIntersec[itIntersec]);
                     if(_controllo[itIntersec]==1)
                     {
                         it_punti++;
                         Point vett2=point.Dif(*it_punti,segment[0]);
                         it_punti--;
                         if(intersec == _intersections.end() || intersec == _intersections.begin())
                         {
                              contatoreVer++;
                         }
                         else if(intersector.VectorialProduct(vettSeg,vett2)>toll && flag2== true)
                         {
                              flag = false;
                              contatoreVer++;
                         }
                         else if(intersector.VectorialProduct(vettSeg,vett2)<-toll && flag2==false)
                         {
                              flag = false;
                              contatoreVer++;
                         }
                         else if(intersector.VectorialProduct(vettSeg,vett2)<toll && intersector.VectorialProduct(vettSeg,vett2)>-toll)
                         { //il punto successivo è ancora un'intersezione
                             flag = false;
                             contatoreVer++;
                             it_numeri++;
                             it_numeri++;
                             counter.push_back(*it_numeri);
                             it_numeri--;
                             it_numeri--;
                         }
                     }
                 }while(flag==false);
                 it_punti++;
                 it_numeri++;
                 vertice=*it_numeri;
             } //fine if
             if(vertice>=numtotVertices)//per l'ultimo lato
                 vertice=0;
         }while(vertice!=verticeinizio);
         subPolygonId++;
         it_punti = _newPoints.begin();
         it_numeri = _numNewPoints.begin();
         it_count= counter.begin();
         do
         {
             it_numeri++;
             it_punti++;
         } while(*it_count != *it_numeri);
         vertice= *it_count;
         counter.pop_front();//rimuoviamo l'elemento dopo averlo  messo in "vertice"
    }while (contatoreVer < numtotVertices);
//se i punti del segmento sono dentro al poligono i flag sono uguali a 1 e quindi pushiamo i punti nei newPoints
    if(flagSeg[0] == 1) _newPoints.push_back(segment[0]);
    if(flagSeg[1] == 1) _newPoints.push_back(segment[1]);
return _cuttedPolygons;
}


unordered_map<int, vector<int>> Cutter::GetCutConvex(const vector<Point>& vertices, const vector<Point> &segment, const vector<int>& vertexPosition)
{
      Intersector intersector;
      unsigned int subPolygonId = 0;
      unsigned int subPolygonId1 = 1;
      vector<int>& cuttedPolygons = _cuttedPolygons[subPolygonId];//vettore estratto dalla mappa
      vector<int>& cuttedPolygons1 = _cuttedPolygons[subPolygonId1];//vettore estratto dalla mappa
      unsigned int numVertices = vertices.size();
      unsigned int counter = 0;//contatore delle intersezioni
      unsigned int i = 0; //per il numero di ogni punto
      intersector.SetSegment(segment[0],segment[1]);
      Point Seg0(segment[0].x,segment[0].y);
      Point Seg1(segment[1].x,segment[1].y);

      for (unsigned int k=0; k<numVertices; k++)
      {
        _newPoints.push_back(vertices[k]);//pushiamo i vertici
      }
      int numIntersec = vertices.size();//numero da cui iniziamo a contare le intersezioni
      do
      {
          cuttedPolygons.push_back(vertexPosition[i]);//pushiamo il vertice iniziale
          intersector.SetEdge(vertices[i], vertices[i+1]);
          if(counter == 0)
          {
              if(intersector.ComputeIntersection() == true)
              {
                  counter = 1;
                  Point pointInt1(intersector.IntersectionCoordinate(segment[0],segment[1]));
                  if(intersector.positionIntersectionEdge != Position::Begin && intersector.positionIntersectionEdge != Position::End)
                  {
                      _newPoints.push_back(pointInt1);
                      cuttedPolygons1.push_back(numIntersec);
                      cuttedPolygons.push_back(numIntersec);
                      numIntersec++;
                  }
                  else//intersezione su un vertice, non la pushiamo nei newPoints
                  {
                      i++;
                      cuttedPolygons.push_back(vertexPosition[i]);
                      cuttedPolygons1.push_back(vertexPosition[i]);
                  }
                  do
                  {
                      i++;
                      cuttedPolygons1.push_back(vertexPosition[i]);//vertice che si trova al di là dell'intersezione
                      intersector.SetEdge(vertices[i], vertices[i+1]);
                  }while(intersector.ComputeIntersection() == false);
                  Point pointInt2(intersector.IntersectionCoordinate(segment[0],segment[1]));
                  unsigned int k=0;
                  unsigned int flag=0;
                  if(intersector.positionIntersectionEdge != Position::Begin && intersector.positionIntersectionEdge != Position::End)
                  {
                      _newPoints.push_back(pointInt2);
                      k=numIntersec;//servirà dopo per "chiudere" il primo sottopoligono
                      flag=1;
                      cuttedPolygons1.push_back(numIntersec);
                  }

                  //controlliamo se i punti del segmento sono dentro al poligono
                  if((Seg0 < pointInt1 && pointInt2 < Seg0) || (Seg0 < pointInt2 && pointInt1 < Seg0))
                   {
                       numIntersec++;
                       cuttedPolygons1.push_back(numIntersec);
                       _newPoints.push_back(segment[0]);
                   }
                   if((Seg1 < pointInt1 && pointInt2 < Seg1) || (Seg1 < pointInt2 && pointInt1 < Seg1))
                   {
                       numIntersec++;
                       cuttedPolygons1.push_back(numIntersec);
                       cuttedPolygons.push_back(numIntersec);
                       _newPoints.push_back(segment[1]);
                   }
                       numIntersec--;
                       cuttedPolygons.push_back(numIntersec);
                       //aggiunto dopo perché nel primo sottopoligono prima ci va il punto del segmento con numero
                       //più alto e dopo quello più basso


                  if(flag == 1)
                  cuttedPolygons.push_back(k);
              }
          }
          i++;
      }while(i != numVertices);
      return _cuttedPolygons;
}


}
