#ifndef CUTTER_HPP
#define CUTTER_HPP

#include "Eigen"
#include "Intersector.hpp"
#include <unordered_map>

using namespace std;
using namespace Eigen;

namespace PolygonCut{

class Cutter : public Intersector
{
private:
    int _numSeg0; //intero corrispondente al primo punto del segmento
    int _numSeg1; //intero corrispondente al secondo punto del segmento
    unordered_map<int, vector<int>> _cuttedPolygons;
    list<Point> _newPoints;
    list<int> _numNewPoints;
    list<Point> _intersections;
    vector <int> _numIntersec;
    vector <int> _controllo; //se le intersezioni sono un vertice metto (1), se no metto (0)


public:
    Cutter(){};

    unordered_map<int, vector<int>> GetCutConvex(const vector<Point>& vertices, const vector<Point>& segment, const vector<int>& vertexPosition);

    unordered_map<int, vector<int>> GetCutGeneral(const vector<Point>& vertices, const vector<int>vertexPosition, const vector<Point>& segment);

    void GetIntersections(const vector<Point>& vertices, const vector<int>& vertexPosition, const vector<Point> &segment);

    list<Point> GetNewPointsReturn() {return _newPoints;}//metodo di supporto che ritorna i newPoints
};
}

#endif // CUTTER_HPP

