#include "Intersector.hpp"

namespace PolygonCut {
Intersector::Intersector()
{
  toll= 1.0E-7;
  _resultParametricCoordinates.setZero(2);
}

bool Intersector::ComputeIntersection()
{
  double parallelism = _matrixTangentVector.determinant();
  bool intersection = false;

  double check = toll * toll * _matrixTangentVector.col(0).squaredNorm() *  _matrixTangentVector.col(1).squaredNorm();
  //si usa la norma al quadrato per evitare la radice che è un'operazione costosa
  //quindi, dati due vettori v e w: check = tolleranza^2 * ||v||^2 * ||w||^2

  if(parallelism * parallelism >= check) //i due vettori direzione non sono paralleli
  {
    Matrix2d solverMatrix; //sarebbe l'inversa di _matrixTangentVector
    solverMatrix << _matrixTangentVector(1,1), -_matrixTangentVector(0,1), -_matrixTangentVector(1,0), _matrixTangentVector(0,0);
    _resultParametricCoordinates = solverMatrix * _rightHandSide; // s = A^-1 * b
    _resultParametricCoordinates /= parallelism;//dividiamo perché abbiamo preso la matrice inversa
   if (_resultParametricCoordinates(1) > -toll  && _resultParametricCoordinates(1)-1.0 < toll)//0<s2<1
        intersection = true;
  }

  else //i due vettori direzione sono paralleli
  {
    //determinante della matrice avente come prima colonna la direzione della retta
    //e come seconda colonna _rightHandSide
    double parallelism2 = fabs(_matrixTangentVector(0,0) * _rightHandSide.y() - _rightHandSide.x() * _matrixTangentVector(1,0));

    double squaredNormFirstEdge = _matrixTangentVector.col(0).squaredNorm();
    double check2 = toll * toll * squaredNormFirstEdge * _rightHandSide.squaredNorm();
    if( parallelism2 * parallelism2 <= check2 )//direzione della retta e _rightHandSide sono paralleli
    {
      double tempNorm = 1.0/squaredNormFirstEdge;

      _resultParametricCoordinates(0) = _matrixTangentVector.col(0).dot(_rightHandSide) * tempNorm ;
      _resultParametricCoordinates(1) = _resultParametricCoordinates(0) - _matrixTangentVector.col(0).dot(_matrixTangentVector.col(1)) * tempNorm;

      intersection = true;

      if(_resultParametricCoordinates(1) < _resultParametricCoordinates(0))
      {
        double tmp = _resultParametricCoordinates(0);
        _resultParametricCoordinates(0) = _resultParametricCoordinates(1);
        _resultParametricCoordinates(1) = tmp;
      }
    }
  }

  //Da qui in poi controlliamo la posizione dell'intersezione

    if(_resultParametricCoordinates(1) < -toll || _resultParametricCoordinates(1) > 1.0 + toll)//s2<0 o s2>1
      {
        positionIntersectionEdge =  Position::Outer;
        intersection = false;
      }

    else if((_resultParametricCoordinates(1) > -toll) && (_resultParametricCoordinates(1) < toll))//s2==0
    {
      _resultParametricCoordinates(1) = 0.0;
      positionIntersectionEdge = Position::Begin;
    }
    else if ((_resultParametricCoordinates(1) > 1.0 - toll) && (_resultParametricCoordinates(1) <= 1.0 + toll))//s2==1
    {
      _resultParametricCoordinates(1) = 1.0;
      positionIntersectionEdge = Position::End;
    }
    else
      positionIntersectionEdge = Position::Inner;
  return intersection;
}

void Intersector::SetSegment(const Point& origin, const Point& end)
{

    Point difference=point.Dif(end,origin);//vettore "direzione" del segmento
    Vector2d temp(difference.x,difference.y);
    _matrixTangentVector.col(0) = temp;
    Vector2d temp2(origin.x,origin.y);
    _originSegment = temp2;

}

void Intersector::SetEdge(const Point& origin, const Point& end)
{
    Point differenze=point.Dif(origin,end);//vettore "direzione" del lato cambiato di segno
    Vector2d temp(differenze.x,differenze.y);
    _matrixTangentVector.col(1) = temp;
    Vector2d temp2(origin.x,origin.y);
    _rightHandSide = temp2 - _originSegment;//vettore differenza tra punto di origine del lato e punto di origine
                                            //del segmento
}

Point Intersector::IntersectionCoordinate(const Point& origin,const Point& end)//sostituiamo il valore di s
                                                                               //nella retta e troviamo le
                                                                          //coordinate del punto di intersezione
{
    Vector2d temp(end.x,end.y);
    Vector2d temp2(origin.x,origin.y);
    Vector2d result((1 - _resultParametricCoordinates(0))* temp2 + _resultParametricCoordinates(0) *temp );
    Point _point(result(0),result(1));
    return(_point);
}

double Intersector::VectorialProduct(const Point& point1, const Point& point2)
{
    double product = point1.x*point2.y - point1.y*point2.x;
    return product;
}
}
