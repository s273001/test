#include "test_rectangle.hpp"
#include "test_concavepolygon.hpp"
#include "test_pentagon.hpp"
#include "test_intersector.hpp"
#include "test_point.hpp"
#include "test_concavepolygon2.hpp"
#include "test_concavepolygon3.hpp"
#include "test_concavepolygon4.hpp"


#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
