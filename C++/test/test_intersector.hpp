#ifndef TEST_INTERSECTOR_H
#define TEST_INTERSECTOR_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Intersector.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace CutterTesting {


TEST(TestIntersector, TestIntersection)
{
  Point a(1.0000e+00, 1.0000e+00);
  Point b(5.0000e+00, 1.0000e+00);
  Point c(2.0000e+00, 1.2000e+00);
  Point d(4.0000e+00, 3.0000e+00);

  Intersector intersector;
  intersector.SetEdge(a, b);
  intersector.SetSegment(c, d);
  EXPECT_TRUE(intersector.ComputeIntersection());
  Point e(5.0000e+00, 1.0000e+00);
  Point f(5.0000e+00, 3.1000e+00);
  intersector.SetEdge(e,f);
  EXPECT_FALSE(intersector.ComputeIntersection());
}

TEST(TestIntersector, TestIntersecRet)
{
  Point a(2.0000e+00, 1.2000e+00);
  Point b(4.0000e+00, 3.0000e+00);
  Point c(1.0000e+00, 1.0000e+00);
  Point d(5.0000e+00, 1.0000e+00);

  Intersector intersector;

  intersector.SetSegment(a, b);
  intersector.SetEdge(c, d);
  EXPECT_TRUE(intersector.ComputeIntersection());
  EXPECT_EQ(intersector.IntersectionCoordinate(a,b).y, 1.0000e+00);
  EXPECT_TRUE(abs(intersector.IntersectionCoordinate(a,b).x- 1.77778) < 1e-4);

}

TEST(TestIntersector, TestVectorialProduct)
{
    Point a(1.0000e+00, 1.0000e+00);
    Point b(5.0000e+00, 1.0000e+00);
    Intersector intersector;
    EXPECT_EQ(intersector.VectorialProduct(a,b),-4.0000e+00);
}

}
#endif //TEST_INTERSECTOR_H
