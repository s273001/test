#ifndef TEST_CONCAVEPOLYGON_H
#define TEST_CONCAVEPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Cutter.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace ConcavePolygonTesting {


void FillConcavePolygonVertices(vector<Point>& vertices) //Creo il vettore di vertici del poligono concavo
{
    vertices.reserve(6);
    vertices.push_back(Point(1.5000e+00, 1.0000e+00));
    vertices.push_back(Point(5.6000e+00, 1.5000e+00));
    vertices.push_back(Point(5.5000e+00, 4.8000e+00));
    vertices.push_back(Point(4.0000e+00, 6.2000e+00));
    vertices.push_back(Point(3.2000e+00, 4.2000e+00));
    vertices.push_back(Point(1.0000e+00, 4.0000e+00));
}
void FillVertexPosition(vector<int>& vertexPosition)
{
    vertexPosition.reserve(6);
    vertexPosition.push_back(int(0));
    vertexPosition.push_back(int(1));
    vertexPosition.push_back(int(2));
    vertexPosition.push_back(int(3));
    vertexPosition.push_back(int(4));
    vertexPosition.push_back(int(5));
}
void FillSegment(vector<Point>& segment)
{
    segment.reserve(2);
    segment.push_back(Point(2.0000e+00, 3.7000e+00));
    segment.push_back(Point(4.1000e+00, 5.9000e+00));
}

void FillNewPoints(list<Point>& newPoints)
{
    newPoints.push_back(Point(1.5000e+00, 1.0000e+00));
    newPoints.push_back(Point(5.6000e+00, 1.5000e+00));
    newPoints.push_back(Point(5.5000e+00, 4.8000e+00));
    newPoints.push_back(Point(4.20433e+00, 6.00929e+00));
    newPoints.push_back(Point(4.0000e+00, 6.2000e+00));
    newPoints.push_back(Point(3.72131e+00, 5.50328e+00));
    newPoints.push_back(Point(3.2000e+00, 4.2000e+00));
    newPoints.push_back(Point(2.40860e+00, 4.12805e+00));
    newPoints.push_back(Point(1.0000e+00, 4.0000e+00));
    newPoints.push_back(Point(1.19122e+00, 2.85270e+00));
    newPoints.push_back(Point(2.0000e+00, 3.7000e+00)); //S1 e S2
    newPoints.push_back(Point(4.1000e+00, 5.9000e+00));

}
void FillCuttedPolygons(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,1,2,6,11,7,4,8,10,9}});
    cuttedPolygons.insert({1,{3,7,11,6}});
    cuttedPolygons.insert({2,{5,9,10,8}});
}

TEST(TestConcavePolygon, TestGetCutGeneral)
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillConcavePolygonVertices(vertices);
    FillVertexPosition(vertexPosition);
    FillSegment(segment);
    FillCuttedPolygons(cuttedPolygons1);
    Cutter cutter;

   cutter.GetIntersections(vertices,vertexPosition,segment);
   unordered_map<int, vector<int>> cuttedPolygons2=cutter.GetCutGeneral(vertices,vertexPosition,segment);
   vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[0];
   vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[0];

   for(unsigned int j=0; j<10; j++)
   {
       const int &point1 = cuttedPolygonsVect1[j];
       const int &point2 = cuttedPolygonsVect2[j];
       EXPECT_EQ(point1, point2);
   }
   for(unsigned int i=1; i<3; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<4; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }

}

TEST(TestConcavePolygon, TestGetNewPoints)
{
      vector<Point> vertices;
      vector<int> vertexPosition;
      vector<Point> segment;
      list<Point> newPoints;
      FillConcavePolygonVertices(vertices);
      FillVertexPosition(vertexPosition);
      FillSegment(segment);
      FillNewPoints(newPoints);
      Cutter cutter;
      cutter.GetIntersections(vertices,vertexPosition,segment);
      cutter.GetCutGeneral(vertices,vertexPosition,segment);
      list<Point> newPoints2=cutter.GetNewPointsReturn();
      list<Point>::iterator p2= newPoints2.begin();
      list<Point>::iterator p1= newPoints.begin();
      double toll= 1.0e-5;
      for(unsigned int i=0; i<12; i++)
      {
           Point point1(*p1);
           Point point2(*p2);
           EXPECT_TRUE(abs(point1.x-point2.x) < toll);
           EXPECT_TRUE(abs(point1.y-point2.y) < toll);
           p1++;
           p2++;
      }
}
}
#endif // TEST_CONCAVEPOLYGON_H
