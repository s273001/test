#ifndef TEST_POINT_H
#define TEST_POINT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace PointTesting {

TEST(TestPoint,TestDif)
{
    const Point& p1=Point(1.0000e+00,1.0000e+00);
    const Point& p2=Point(5.0000e+00,1.0000e+00);
    Point p3;
    EXPECT_EQ(Point(-4.0000e+00,0.0000e+00),p3.Dif(p1,p2));
}

}
#endif // TEST_POINT_H
