#ifndef TEST_PENTAGON_H
#define TEST_PENTAGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Cutter.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace PentagonTesting {

void FillPentagonVertices(vector<Point>& vertices) //Creo il vettore di vertici del pentagono
    {
      vertices.reserve(5);
      vertices.push_back(Point(2.5000e+00, 1.0000e+00));
      vertices.push_back(Point(4.0000e+00, 2.1000e+00));
      vertices.push_back(Point(3.4000e+00, 4.2000e+00));
      vertices.push_back(Point(1.6000e+00, 4.2000e+00));
      vertices.push_back(Point(1.0000e+00, 2.1000e+00));
    }
void FillVertexPosition(vector<int>& vertexPosition)
  {
      vertexPosition.reserve(5);
      vertexPosition.push_back(int(0));
      vertexPosition.push_back(int(1));
      vertexPosition.push_back(int(2));
      vertexPosition.push_back(int(3));
      vertexPosition.push_back(int(4));
  }
void FillSegment(vector<Point>& segment)
  {
      segment.reserve(2);
      segment.push_back(Point(1.4000e+00, 2.7500e+00));
      segment.push_back(Point(3.6000e+00, 2.2000e+00));
  }



void FillNewPoints(list<Point>& newPoints)
{
    newPoints.push_back(Point(2.5000e+00, 1.0000e+00)); //5 vertici
    newPoints.push_back(Point(4.0000e+00, 2.1000e+00));
    newPoints.push_back(Point(3.4000e+00, 4.2000e+00));
    newPoints.push_back(Point(1.6000e+00, 4.2000e+00));
    newPoints.push_back(Point(1.0000e+00, 2.1000e+00));
    newPoints.push_back(Point(1.2000e+00, 2.8000e+00)); //Intersezione (l'altra coincide col vertice 1)
    newPoints.push_back(Point(1.4000e+00, 2.7500e+00));//S1 e S2
    newPoints.push_back(Point(3.6000e+00, 2.2000e+00));

}

void FillCuttedPolygons(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,1,7,6,5,4}});
    cuttedPolygons.insert({1,{1,2,3,5,6,7}});
}

TEST(TestPentagon, TestGetCutConvex) //TestGetCutConvex poligoni convessi
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillPentagonVertices(vertices);
    FillVertexPosition(vertexPosition);
    FillSegment(segment);
    FillCuttedPolygons(cuttedPolygons1);
    Cutter cutter;
    unordered_map<int, vector<int>> cuttedPolygons2 = cutter.GetCutConvex(vertices,segment,vertexPosition);
    for(unsigned int i=0; i<2; i++)
    {
        vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
        vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
        for(unsigned int j=0; j<6; j++)
        {
            const int &point1 = cuttedPolygonsVect1[j];
            const int &point2 = cuttedPolygonsVect2[j];
            EXPECT_EQ(point1, point2);
        }
    }
}

TEST(TestPentagon, TestGetNewPointsReturn) //TestGetCutConvex poligoni convessi
{
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    list<Point> newPoints1;
    FillPentagonVertices(vertices);
    FillVertexPosition(vertexPosition);
    FillSegment(segment);
    FillNewPoints(newPoints1);
    Cutter cutter;
    cutter.GetCutConvex(vertices,segment,vertexPosition);
    list<Point> newPoints2=cutter.GetNewPointsReturn();
    list<Point>::iterator p2= newPoints2.begin();
    list<Point>::iterator p1= newPoints1.begin();
    double toll= 1.0e-5;
    for(unsigned int i=0; i<8; i++)
    {
         Point point1(*p1);
         Point point2(*p2);
         EXPECT_TRUE(abs(point1.x-point2.x) < toll);
         EXPECT_TRUE(abs(point1.y-point2.y) < toll);
         p1++;
         p2++;
    }
}
}
#endif // TEST_PENTAGON_H
