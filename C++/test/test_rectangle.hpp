#ifndef TEST_RECTANGLE_H
#define TEST_RECTANGLE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Cutter.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace RectangleTesting {


void FillRectangleVertices(vector<Point>& vertices) //Creo il vettore di vertici del rettangolo
{
    vertices.reserve(4);
    vertices.push_back(Point(1.0000e+00, 1.0000e+00));
    vertices.push_back(Point(5.0000e+00, 1.0000e+00));
    vertices.push_back(Point(5.0000e+00, 3.1000e+00));
    vertices.push_back(Point(1.0000e+00, 3.1000e+00));
}
void FillVertexPosition(vector<int>& vertexPosition)
{
    vertexPosition.reserve(4);
    vertexPosition.push_back(int(0));
    vertexPosition.push_back(int(1));
    vertexPosition.push_back(int(2));
    vertexPosition.push_back(int(3));
}
void FillSegment(vector<Point>& segment)
{
    segment.reserve(2);
    segment.push_back(Point(2.0000e+00, 1.2000e+00));
    segment.push_back(Point(4.0000e+00, 3.0000e+00));
}
void FillNewPoints(list<Point>& newPoints)
{
    newPoints.push_back(Point(1.0000e+00, 1.0000e+00)); //4 vertici
    newPoints.push_back(Point(5.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(5.0000e+00, 3.1000e+00));
    newPoints.push_back(Point(1.0000e+00, 3.1000e+00));
    newPoints.push_back(Point(1.77778e+00, 1.00000e+00)); //2 intersezioni
    newPoints.push_back(Point(4.11111e+00, 3.10000e+00));
    newPoints.push_back(Point(2.0000e+00, 1.2000e+00));// S1 e S2
    newPoints.push_back(Point(4.0000e+00, 3.0000e+00));
}

void FillCuttedPolygons(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,4,7,6,5,3}});
    cuttedPolygons.insert({1,{4,1,2,5,6,7}});
}

TEST(TestRectangle, TestGetCutConvex) //TestGetCutConvex poligoni convessi
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillRectangleVertices(vertices);
    FillVertexPosition(vertexPosition);
    FillSegment(segment);
    FillCuttedPolygons(cuttedPolygons1);
    Cutter cutter;
    unordered_map<int, vector<int>> cuttedPolygons2 = cutter.GetCutConvex(vertices,segment,vertexPosition);
    for(unsigned int i=0; i<2; i++)
    {
        vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
        vector< int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
        for(unsigned int j=0; j<6; j++)
        {
            const int &point1 = cuttedPolygonsVect1[j];
            const int &point2 = cuttedPolygonsVect2[j];
            EXPECT_EQ(point1, point2);
        }
    }

}

TEST(TestRectangle, TestNewPointsReturn)
{
      vector<Point> vertices;
      vector<int> vertexPosition;
      vector<Point> segment;
      list<Point> newPoints1;
      FillRectangleVertices(vertices);
      FillVertexPosition(vertexPosition);
      FillSegment(segment);
      FillNewPoints(newPoints1);
      Cutter cutter;
      cutter.GetCutConvex(vertices,segment,vertexPosition);
      list<Point> newPoints2=cutter.GetNewPointsReturn();
      list<Point>::iterator p2= newPoints2.begin();
      list<Point>::iterator p1= newPoints1.begin();
      double toll= 1.0e-5;
      for(unsigned int i=0; i<8; i++)
      {
           Point point1(*p1);
           Point point2(*p2);
           EXPECT_TRUE(abs(point1.x-point2.x) < toll);
           EXPECT_TRUE(abs(point1.y-point2.y) < toll);
           p1++;
           p2++;
      }


}

}
#endif // TEST_RECTANGLE_H
