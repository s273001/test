#ifndef TEST_CONCAVEPOLYGON3_H
#define TEST_CONCAVEPOLYGON3_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Cutter.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace ConcavePolygonTesting {


void FillConcavePolygonVertices3(vector<Point>& vertices) //Creo il vettore di vertici del poligono concavo
{
    vertices.reserve(8);
    vertices.push_back(Point(-1.0000e+00, 1.0000e+00));
    vertices.push_back(Point(3.0000e+00, 1.0000e+00));
    vertices.push_back(Point(4.0000e+00, 4.0000e+00));
    vertices.push_back(Point(2.0000e+00, 4.0000e+00));
    vertices.push_back(Point(2.5000e+00, 3.3000e+00));
    vertices.push_back(Point(0.5000e+00, 3.0000e+00));
    vertices.push_back(Point(0.0000e+00, 5.0000e+00));
    vertices.push_back(Point(-1.3000e+00, 4.7000e+00));
}
void FillVertexPosition3(vector<int>& vertexPosition)
{
    vertexPosition.reserve(8);
    vertexPosition.push_back(int(0));
    vertexPosition.push_back(int(1));
    vertexPosition.push_back(int(2));
    vertexPosition.push_back(int(3));
    vertexPosition.push_back(int(4));
    vertexPosition.push_back(int(5));
    vertexPosition.push_back(int(6));
    vertexPosition.push_back(int(7));
}
void FillSegment3(vector<Point>& segment)
{
    segment.reserve(2);
    segment.push_back(Point(0.877356361e+00,3.0566034541e+00));
    segment.push_back(Point(4.988014965e+00, 3.6732022448e+00));
}

void FillNewPoints3(list<Point>& newPoints)
{
    newPoints.push_back(Point(-1.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(3.833333e+00, 3.5000e+00));
    newPoints.push_back(Point(4.0000e+00, 4.0000e+00));
    newPoints.push_back(Point(2.0000e+00, 4.0000e+00));
    newPoints.push_back(Point(2.5000e+00, 3.3000e+00));
    newPoints.push_back(Point(0.5000e+00, 3.0000e+00));
    newPoints.push_back(Point(0.0000e+00, 5.0000e+00));
    newPoints.push_back(Point(-1.3000e+00, 4.7000e+00));
    newPoints.push_back(Point(-1.142189e+00, 2.753671e+00));
    newPoints.push_back(Point(0.877356361e+00,3.0566034541e+00));

}
void FillCuttedPolygons3(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,1,8,4,10,5,9}});
    cuttedPolygons.insert({1,{2,3,4,8}});
    cuttedPolygons.insert({2,{6,7,9,5}});
}

TEST(TestConcavePolygon3, TestGetCutGeneral)
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillConcavePolygonVertices3(vertices);
    FillVertexPosition3(vertexPosition);
    FillSegment3(segment);
    FillCuttedPolygons3(cuttedPolygons1);
    Cutter cutter;

   cutter.GetIntersections(vertices,vertexPosition,segment);
   unordered_map<int, vector< int>> cuttedPolygons2=cutter.GetCutGeneral(vertices,vertexPosition,segment);
   vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[0];
   vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[0];

   for(unsigned int j=0; j<7; j++)
   {
       const int &point1 = cuttedPolygonsVect1[j];
       const int &point2 = cuttedPolygonsVect2[j];
       EXPECT_EQ(point1, point2);
   }
   for(unsigned int i=1; i<3; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<4; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }

}

TEST(TestConcavePolygon3, TestGetNewPoints)
{
      vector<Point> vertices;
      vector<int> vertexPosition;
      vector<Point> segment;
      list<Point> newPoints;
      FillConcavePolygonVertices3(vertices);
      FillVertexPosition3(vertexPosition);
      FillSegment3(segment);
      FillNewPoints3(newPoints);
      Cutter cutter;
      cutter.GetIntersections(vertices,vertexPosition,segment);
      cutter.GetCutGeneral(vertices,vertexPosition,segment);
      list<Point> newPoints2=cutter.GetNewPointsReturn();
      list<Point>::iterator p2= newPoints2.begin();
      list<Point>::iterator p1= newPoints.begin();
      double toll= 1.0e-4;
      for(unsigned int i=0; i<11; i++)
      {
           Point point1(*p1);
           Point point2(*p2);
           EXPECT_TRUE(abs(point1.x-point2.x) < toll);
           EXPECT_TRUE(abs(point1.y-point2.y) < toll);
           p1++;
           p2++;
      }
}
}
#endif // TEST_CONCAVEPOLYGON3_H
