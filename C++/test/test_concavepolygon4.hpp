#ifndef TEST_CONCAVEPOLYGON4_H
#define TEST_CONCAVEPOLYGON4_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Cutter.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace ConcavePolygonTesting {


void FillConcavePolygonVertices4(vector<Point>& vertices) //Creo il vettore di vertici del poligono concavo
{
    vertices.reserve(13);
    vertices.push_back(Point(3.0000e+00, 0.0000e+00));
    vertices.push_back(Point(2.0000e+00, 0.0000e+00));
    vertices.push_back(Point(2.0000e+00, 3.0000e+00));
    vertices.push_back(Point(-2.0000e+00, 1.0000e+00));
    vertices.push_back(Point(-3.0000e+00, 2.0000e+00));
    vertices.push_back(Point(-3.0000e+00, -4.0000e+00));
    vertices.push_back(Point(-1.0000e+00, -5.0000e+00));
    vertices.push_back(Point(-2.0000e+00, -2.0000e+00));
    vertices.push_back(Point(1.0000e+00, 1.0000e+00));
    vertices.push_back(Point(2.0000e+00, -4.0000e+00));
    vertices.push_back(Point(4.0000e+00, -1.0000e+00));
    vertices.push_back(Point(5.0000e+00, 3.0000e+00));
    vertices.push_back(Point(3.0000e+00, 4.0000e+00));
}

void FillVertexPosition4(vector<int>& vertexPosition)
{
    vertexPosition.reserve(13);
    vertexPosition.push_back(int(0));
    vertexPosition.push_back(int(1));
    vertexPosition.push_back(int(2));
    vertexPosition.push_back(int(3));
    vertexPosition.push_back(int(4));
    vertexPosition.push_back(int(5));
    vertexPosition.push_back(int(6));
    vertexPosition.push_back(int(7));
    vertexPosition.push_back(int(8));
    vertexPosition.push_back(int(9));
    vertexPosition.push_back(int(10));
    vertexPosition.push_back(int(11));
    vertexPosition.push_back(int(12));
}

void FillSegment4(vector<Point>& segment)
{
    segment.reserve(2);
    segment.push_back(Point(0.0000e+00, 0.0000e+00));
    segment.push_back(Point(3.5000e+00, 3.5000e+00));
}

void FillNewPoints4(list<Point>& newPoints)
{
    newPoints.push_back(Point(3.0000e+00, 0.0000e+00));
    newPoints.push_back(Point(2.0000e+00, 0.0000e+00));
    newPoints.push_back(Point(2.0000e+00, 2.0000e+00));
    newPoints.push_back(Point(2.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(-2.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, 2.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, -3.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, -4.0000e+00));
    newPoints.push_back(Point(-1.0000e+00, -5.0000e+00));
    newPoints.push_back(Point(-2.0000e+00, -2.0000e+00));
    newPoints.push_back(Point(1.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(2.0000e+00, -4.0000e+00));
    newPoints.push_back(Point(4.0000e+00, -1.0000e+00));
    newPoints.push_back(Point(5.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(3.666666e+00, 3.66666e+00));
    newPoints.push_back(Point(3.0000e+00, 4.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(0.0000e+00, 0.0000e+00));
    newPoints.push_back(Point(3.5000e+00, 3.5000e+00));

}
void FillCuttedPolygons4(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,1,13,8,9,10,11,15,18,16}});
    cuttedPolygons.insert({1,{2,3,4,14,7,17,8,13}});
    cuttedPolygons.insert({2,{12,16,18,15}});
    cuttedPolygons.insert({3,{5,6,7,14}});
}

TEST(TestConcavePolygon4, TestGetCutGeneral)
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillConcavePolygonVertices4(vertices);
    FillVertexPosition4(vertexPosition);
    FillSegment4(segment);
    FillCuttedPolygons4(cuttedPolygons1);
    Cutter cutter;

   cutter.GetIntersections(vertices,vertexPosition,segment);
   unordered_map<int, vector<int>> cuttedPolygons2=cutter.GetCutGeneral(vertices,vertexPosition,segment);
   vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[0];
   vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[0];

   for(unsigned int j=0; j<10; j++)
   {
       const int &point1 = cuttedPolygonsVect1[j];
       const int &point2 = cuttedPolygonsVect2[j];
       EXPECT_EQ(point1, point2);
   }
   for(unsigned int i=1; i<2; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<8; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
   for(unsigned int i=2; i<4; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<4; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }


}

TEST(TestConcavePolygon4, TestGetNewPoints)
{
      vector<Point> vertices;
      vector<int> vertexPosition;
      vector<Point> segment;
      list<Point> newPoints;
      FillConcavePolygonVertices4(vertices);
      FillVertexPosition4(vertexPosition);
      FillSegment4(segment);
      FillNewPoints4(newPoints);
      Cutter cutter;
      cutter.GetIntersections(vertices,vertexPosition,segment);
      cutter.GetCutGeneral(vertices,vertexPosition,segment);
      list<Point> newPoints2=cutter.GetNewPointsReturn();
      list<Point>::iterator p2= newPoints2.begin();
      list<Point>::iterator p1= newPoints.begin();
      double toll= 1.0e-4;
      for(unsigned int i=0; i<19; i++)
      {
           Point point1(*p1);
           Point point2(*p2);
           EXPECT_TRUE(abs(point1.x-point2.x) < toll);
           EXPECT_TRUE(abs(point1.y-point2.y) < toll);
           p1++;
           p2++;
      }
}
}
#endif // TEST_CONCAVEPOLYGON4_H
