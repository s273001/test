#ifndef TEST_CONCAVEPOLYGON2_H
#define TEST_CONCAVEPOLYGON2_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Cutter.hpp"
#include "Point.hpp"

using namespace PolygonCut;
using namespace testing;
using namespace std;

namespace ConcavePolygonTesting {


void FillConcavePolygonVertices2(vector<Point>& vertices) //Creo il vettore di vertici del poligono concavo
{
    vertices.reserve(10);
    vertices.push_back(Point(2.0000e+00, -2.0000e+00));
    vertices.push_back(Point(0.0000e+00, -1.0000e+00));
    vertices.push_back(Point(3.0000e+00, 1.0000e+00));
    vertices.push_back(Point(0.0000e+00, 2.0000e+00));
    vertices.push_back(Point(3.0000e+00, 2.0000e+00));
    vertices.push_back(Point(3.0000e+00, 3.0000e+00));
    vertices.push_back(Point(-1.0000e+00, 3.0000e+00));
    vertices.push_back(Point(-3.0000e+00, 1.0000e+00));
    vertices.push_back(Point(0.0000e+00, 0.0000e+00));
    vertices.push_back(Point(-3.0000e+00, -2.0000e+00));
}
void FillVertexPosition2(vector<int>& vertexPosition)
{
    vertexPosition.reserve(10);
    vertexPosition.push_back(int(0));
    vertexPosition.push_back(int(1));
    vertexPosition.push_back(int(2));
    vertexPosition.push_back(int(3));
    vertexPosition.push_back(int(4));
    vertexPosition.push_back(int(5));
    vertexPosition.push_back(int(6));
    vertexPosition.push_back(int(7));
    vertexPosition.push_back(int(8));
    vertexPosition.push_back(int(9));
}
void FillSegment1(vector<Point>& segment)
{
    segment.reserve(2);
    segment.push_back(Point(-4.0000e+00, -4.0000e+00));
    segment.push_back(Point(4.0000e+00, 4.0000e+00));
}

void FillNewPoints1(list<Point>& newPoints)
{
    newPoints.push_back(Point(2.0000e+00, -2.0000e+00));
    newPoints.push_back(Point(0.0000e+00, -1.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(1.5000e+00, 1.5000e+00));
    newPoints.push_back(Point(0.0000e+00, 2.0000e+00));
    newPoints.push_back(Point(2.0000e+00, 2.0000+00));
    newPoints.push_back(Point(3.0000e+00, 2.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(-1.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(0.0000e+00, 0.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, -2.0000e+00));
    newPoints.push_back(Point(-2.0000e+00, -2.0000e+00));

}

void FillCuttedPolygons1(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,1,2,10,8,12}});
    cuttedPolygons.insert({1,{3,11,5,6,7,8,10}});
    cuttedPolygons.insert({2,{4,5,11}});
    cuttedPolygons.insert({3,{9,12,8}});
}
TEST(TestConcavePolygon1, TestGetCutGeneral)
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillConcavePolygonVertices2(vertices);
    FillVertexPosition2(vertexPosition);
    FillSegment1(segment);
    FillCuttedPolygons1(cuttedPolygons1);
    Cutter cutter;
    Intersector inter;
   cutter.GetIntersections(vertices,vertexPosition,segment);
   unordered_map<int, vector<int>> cuttedPolygons2=cutter.GetCutGeneral(vertices,vertexPosition,segment);

   for(unsigned int i=0; i<1; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<6; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
   for(unsigned int i=1; i<2; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<7; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
   for(unsigned int i=2; i<4; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<3; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
}

TEST(TestConcavePolygon1, TestGetNewPointsReturn)
{
      vector<Point> vertices;
      vector<int> vertexPosition;
      vector<Point> segment;
      list<Point> newPoints;
      FillConcavePolygonVertices2(vertices);
      FillVertexPosition2(vertexPosition);
      FillSegment1(segment);
      FillNewPoints1(newPoints);
      Cutter cutter;
      cutter.GetIntersections(vertices,vertexPosition,segment);
      cutter.GetCutGeneral(vertices,vertexPosition,segment);
      list<Point> newPoints2=cutter.GetNewPointsReturn();
      list<Point>::iterator p2= newPoints2.begin();
      list<Point>::iterator p1= newPoints.begin();
      double toll= 1.0e-4;
      for(unsigned int i=0; i<13; i++)
      {
           Point point1(*p1);
           Point point2(*p2);
           EXPECT_TRUE(abs(point1.x-point2.x) < toll);
           EXPECT_TRUE(abs(point1.y-point2.y) < toll);
           p1++;
           p2++;
      }
}
void FillSegment2(vector<Point>& segment)
{
    segment.reserve(2);
    segment.push_back(Point(0.0000e+00, -3.0000e+00));
    segment.push_back(Point(0.0000e+00, 4.0000e+00));
}

void FillNewPoints2(list<Point>& newPoints)
{
    newPoints.push_back(Point(2.0000e+00, -2.0000e+00));
    newPoints.push_back(Point(0.0000e+00, -1.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(0.0000e+00, 2.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 2.0000e+00));
    newPoints.push_back(Point(3.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(0.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(-1.0000e+00, 3.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, 1.0000e+00));
    newPoints.push_back(Point(0.0000e+00, 0.0000e+00));
    newPoints.push_back(Point(-3.0000e+00, -2.0000e+00));
    newPoints.push_back(Point(0.0000e+00, -2.0000e+00));

}

void FillCuttedPolygons2(unordered_map<int, vector<int>>& cuttedPolygons)
{
    cuttedPolygons.insert({0,{0,1,11}});
    cuttedPolygons.insert({1,{2,3,8,1}});
    cuttedPolygons.insert({2,{4,5,10,3}});
    cuttedPolygons.insert({3,{6,7,8,3,10}});
    cuttedPolygons.insert({4,{9,11,1,8}});
}

TEST(TestConcavePolygon2, TestGetNewPointsReturn)
{
      vector<Point> vertices;
      vector<int> vertexPosition;
      vector<Point> segment;
      list<Point> newPoints;
      FillConcavePolygonVertices2(vertices);
      FillVertexPosition2(vertexPosition);
      FillSegment2(segment);
      FillNewPoints2(newPoints);
      Cutter cutter;
      cutter.GetIntersections(vertices,vertexPosition,segment);
      cutter.GetCutGeneral(vertices,vertexPosition,segment);
      list<Point> newPoints2=cutter.GetNewPointsReturn();
      list<Point>::iterator p2= newPoints2.begin();
      list<Point>::iterator p1= newPoints.begin();
      double toll= 1.0e-4;
      for(unsigned int i=0; i<12; i++)
      {
           Point point1(*p1);
           Point point2(*p2);
           EXPECT_TRUE(abs(point1.x-point2.x) < toll);
           EXPECT_TRUE(abs(point1.y-point2.y) < toll);
           p1++;
           p2++;
      }
}

TEST(TestConcavePolygon2, TestGetCutGeneral)
  {
    vector<Point> vertices;
    vector<int> vertexPosition;
    vector<Point> segment;
    unordered_map<int, vector<int>> cuttedPolygons1;
    FillConcavePolygonVertices2(vertices);
    FillVertexPosition2(vertexPosition);
    FillSegment2(segment);
    FillCuttedPolygons2(cuttedPolygons1);
    Cutter cutter;
    Intersector inter;
   cutter.GetIntersections(vertices,vertexPosition,segment);
   unordered_map<int, vector<int>> cuttedPolygons2=cutter.GetCutGeneral(vertices,vertexPosition,segment);
   vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[0];
   vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[0];


    for(unsigned int j=0; j<3; j++)
    {
       const int &point1 = cuttedPolygonsVect1[j];
       const int &point2 = cuttedPolygonsVect2[j];
       EXPECT_EQ(point1, point2);
   }
   for(unsigned int i=1; i<3; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<4; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
   for(unsigned int i=3; i<4; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector<int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<5; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
   for(unsigned int i=4; i<5; i++)
   {
       vector<int> &cuttedPolygonsVect1 = cuttedPolygons1[i];
       vector< int> &cuttedPolygonsVect2 = cuttedPolygons2[i];
       for(unsigned int j=0; j<4; j++)
       {
           const int &point1 = cuttedPolygonsVect1[j];
           const int &point2 = cuttedPolygonsVect2[j];
           EXPECT_EQ(point1, point2);
       }
   }
}

}
#endif // TEST_CONCAVEPOLYGON2_H
